import { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Candidates } from './src/constants/candidates';
import BallotBoxScreen from './src/screens/BallotBoxScreen';

export default function App() {

  const [candidates, setCandidates] = useState(Candidates)

  return (
    <View style={styles.container}>
      <BallotBoxScreen candidates={candidates} setCandidates={setCandidates}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

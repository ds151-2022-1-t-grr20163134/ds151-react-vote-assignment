import { Button, StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-web";

export default function Ballot({candidate, vote}) {    
    return (
      <View style={styles.container}>
        <Text>{candidate.name}</Text>
        <Text>{candidate.votes}</Text>
        <Button title="Vote" onPress={() => vote(candidate.id)}/>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
    //   flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  
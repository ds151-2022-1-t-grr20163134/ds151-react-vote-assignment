import { useState } from "react";
import { Text, View, StyleSheet, Button, FlatList } from "react-native";
import Ballot from "../components/Ballot";

const BallotBoxScreen = ({candidates, setCandidates}) => {
    
    const [isOver, setIsOver] = useState(false)
    const [totalVotes, setTotalVotes] = useState(0)

    const vote = (id) => {
        let tempCandidates = candidates;
        for(let tempCandidate of tempCandidates){
            if(tempCandidate.id === id){
                tempCandidate.votes++
            }
        }
        setTotalVotes(totalVotes + 1)
        setCandidates(tempCandidates)
    }

    const countVotes = (candidates) => {
        return candidates.find((w) => w.votes == Math.max(...candidates.map(c => c.votes))); 
    }

    if (isOver) {
        let winner = countVotes(candidates)
        return (
            <View>
                <Text>Election is over</Text>
                <Text>{winner.name} won.</Text>
            </View>
        )
    }
    return (
        <View style={style.container}>
            <Text>Title</Text>
            <FlatList
                data={candidates}
                renderItem={({item}) => <Ballot candidate={item} vote={vote}/>}
                keyExtractor={item => item.id} />
            <Button title="End Election" onPress={() => setIsOver(true)} />
        </View>
    );
}

const style = StyleSheet.create({
    container: {
        backgroundColor: '#aaa',
        height: '100%',
        width: '100%',
    }
})

export default BallotBoxScreen